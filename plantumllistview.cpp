#include "plantumllistview.h"
#include "ui_plantumllistview.h"
#include "utils/qrcutil.h"

PlantUMLListView::PlantUMLListView(QsciScintilla *parent)
    : QWidget()
    , s_edit(parent)
    , ui(new Ui::PlantUMLListView)
{
    ui->setupUi(this);

    preview = new PlantUMLPreview(nullptr, s_edit);
    preview->onceCallSetWindowStaysOnTopHint();
    preview->show();

    // 预览窗口销毁时此窗口也销毁
    connect(preview, &QObject::destroyed, this, &QWidget::deleteLater);
    // 编辑器关闭时，此窗口与预览窗口同时关闭
    connect(s_edit, &QObject::destroyed, this, &QWidget::deleteLater);
    connect(s_edit, &QObject::destroyed, preview, &QWidget::deleteLater);


    auto files = QR(":/examples/files.txt");
    files.replace("\r\n", "\n");

#if (QT_VERSION <= QT_VERSION_CHECK(5,14,0))
    auto nodes = files.split("\n\n", QString::SkipEmptyParts);
#else
    auto nodes = files.split("\n\n", Qt::SkipEmptyParts);
#endif
    foreach (auto node, nodes) {

#if (QT_VERSION <= QT_VERSION_CHECK(5,14,0))
    foreach (auto var, node.split("\n", QString::SkipEmptyParts)) {
#else
    foreach (auto var, node.split("\n", Qt::SkipEmptyParts)) {
#endif
            ui->listWidget->addItem(var.replace(":/examples/", ""));
        }
    }
    // ":/examples/0 Docs/GraphViz.puml"
}

PlantUMLListView::~PlantUMLListView()
{
    delete ui;
}

void PlantUMLListView::on_listWidget_currentTextChanged(const QString &currentText)
{
    auto content = QR(":/examples/"+currentText);
    s_edit->setText(content);

    preview->setWindowTitle(currentText);
}

