#ifndef PLANTUMLLISTVIEW_H
#define PLANTUMLLISTVIEW_H

#include "plantumlpreview.h"
#include <QWidget>

namespace Ui {
class PlantUMLListView;
}

class PlantUMLListView : public QWidget
{
    Q_OBJECT

public:
    explicit PlantUMLListView(QsciScintilla *parent = nullptr);
    ~PlantUMLListView();

private slots:
    void on_listWidget_currentTextChanged(const QString &currentText);

private:
    Ui::PlantUMLListView *ui;

    PlantUMLPreview *preview;
    QsciScintilla *s_edit;
};

#endif // PLANTUMLLISTVIEW_H
