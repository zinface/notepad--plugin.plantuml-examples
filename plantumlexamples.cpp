﻿#include "plantumlexamples.h"
#include "plantumllistview.h"
#include "pluginframeworkhelper.h"

#include <plantumlpreview.h>
#include <QDockWidget>
#include <QMessageBox>

#include <utils/qrcutil.h>

PlantumlExamples::PlantumlExamples(QObject *parent)
    : QObject{parent}
{}

PlantumlExamples &PlantumlExamples::instance()
{
    static PlantumlExamples _plugin;
    return _plugin;
}

QString PlantumlExamples::PluginName()
{
    return "PlantUml Examples";
}

QString PlantumlExamples::PluginVersion()
{
    return "v0.1";
}

QString PlantumlExamples::PluginAuthor()
{
    return "zinface";
}

QString PlantumlExamples::PluginComment()
{
    return u8"PlantUML 300示例 插件";
}

IPluginFramework::MenuType PlantumlExamples::PluginMenuType()
{
    return MenuType::SecondaryMenu;
}

void PlantumlExamples::registerNotepad(QWidget *notepad)
{
    s_notepad = notepad;
}

void PlantumlExamples::registerStrFileName(QString str_file_name)
{
    s_str_file_name = str_file_name;
}

void PlantumlExamples::PluginTrigger()
{
    //Trigger: Will never enter here.
}

void PlantumlExamples::registerPluginActions(QMenu *rootMenu)
{
    rootMenu->addAction("Preview All", this, [this](){
        // QMessageBox::information(nullptr, "tip", "This is default tip message.");

        PluginFrameworkHelper::DoNewEdit(s_notepad, s_plugin_callback);
        auto edit = s_get_cur_edit_callback(s_notepad);

        PlantUMLListView *view = new PlantUMLListView(edit);
        connect(edit, &QObject::destroyed, view, &QObject::deleteLater);

        auto dock =new QDockWidget("PlantUML Examples", s_notepad);
        connect(view, &QObject::destroyed, dock, &QObject::deleteLater);

        dock->setAllowedAreas(Qt::DockWidgetArea::LeftDockWidgetArea | Qt::DockWidgetArea::RightDockWidgetArea);
        dock->setWidget(view);


        (dynamic_cast<QMainWindow*>(s_notepad))->addDockWidget(Qt::RightDockWidgetArea, dock);
    });


    auto files = QR(":/examples/files.txt");
    files.replace("\r\n", "\n");

#if (QT_VERSION <= QT_VERSION_CHECK(5,14,0))
    auto nodes = files.split("\n\n", QString::SkipEmptyParts);
#else
    auto nodes = files.split("\n\n", Qt::SkipEmptyParts);
#endif
    foreach (auto node, nodes) {
        auto nodeMenu = new QMenu(rootMenu);
#if (QT_VERSION <= QT_VERSION_CHECK(5,14,0))
        foreach (auto var, node.split("\n", QString::SkipEmptyParts)) {
#else
        foreach (auto var, node.split("\n", Qt::SkipEmptyParts)) {
#endif
            nodeMenu->setTitle(var.split("/")[2]);

            auto ac = nodeMenu->addAction(var.split("/")[3]);
            ac->setProperty("puml", QR(var));
            ac->setProperty("filename", QString("%1 - %2").arg(var.split("/")[2]).arg(var.split("/")[3]));
            ac->connect(ac, &QAction::triggered, [ac, this](){
                PlantUMLPreview *puml = new PlantUMLPreview(nullptr, nullptr);
                puml->setRenderText(((QAction*)ac)->property("puml").toString());
                puml->onceCallSetWindowStaysOnTopHint();
                puml->show();
                puml->setWindowTitle(((QAction*)ac)->property("filename").toString());
                puml->resize(800, 600);

                PluginFrameworkHelper::DoNewEdit(s_notepad, s_plugin_callback);
                auto edit = s_get_cur_edit_callback(s_notepad);
                edit->setText(puml->renderText());
                connect(edit, &QsciScintilla::textChanged, puml, [edit, puml](){
                    puml->setRenderText(edit->text());
                    puml->slotTextChanged();
                });
            });
        }
        rootMenu->addMenu(nodeMenu);
    }
    // ":/examples/0 Docs/GraphViz.puml"

}

void PlantumlExamples::registerCurrentEditCallback(std::function<QsciScintilla *(QWidget *)> get_cur_edit_callback)
{
    s_get_cur_edit_callback = get_cur_edit_callback;
}

void PlantumlExamples::registerPluginCallBack(std::function<bool (QWidget *, int, void *)> plugin_callback)
{
    s_plugin_callback = plugin_callback;
}

// Plug-in implementation wrapper
NDD_DECLARE_PLUGIN(PlantumlExamples::instance())
