﻿# 一个简单的 plantuml-examples 插件
add_framework_plugin(framework-plantuml-examples
    # 这里可以进行编写包含源代码的目录、文件路径、其它需要构建的资源路径
    ${CMAKE_CURRENT_LIST_DIR}
)
if(framework-plantuml-examples_ENABLE)
    # 这里可以进行扩展构建，
    find_package(Qt5Svg)
    target_link_libraries(framework-plantuml-examples Qt5::Svg)

    # 声明通过 with_git 引用 plantuml 插件提供的共享引用构建资源Z
    framework_plugin_include_with_git(framework-plantuml-examples
        https://gitee.com/zinface/notepad--plugin.plantuml-preview
        --branch=cmake-plugins-dev)
    # 声明通过 with_git 引用中央存储库提供的共享引用构建资源
    framework_plugin_include_with_git(framework-plantuml-examples
        https://gitee.com/ndd-community/framework-plugin-component-library)
endif(framework-plantuml-examples_ENABLE)
