# Notepad-- PlantUML Examples

- 在线构建支持(plugin.cmake)

    ```cmake
    # 1. 基于 git 仓库在线构建 plantuml-examples 插件
    add_framework_plugin_with_git(
        https://gitee.com/zinface/notepad--plugin.plantuml-examples 
        --branch=cmake-plugins-dev)
    
    # 将以上代码复制到 src/plugin/dev.cmake 中即可构建
    ```

- 效果图

    ![](assets/20240323.png)

- 使用的数据来源

    https://github.com/mattjhayes/PlantUML-Examples
